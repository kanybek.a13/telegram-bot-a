<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Support\Facades\App;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramController extends Controller
{
    public function getUpdates(){
        $updates = Telegram::getUpdates();

        return (json_encode($updates));
    }

    public function sendMessage(){
        $keyboard_markup = $this->getKeyboard('App\Models\Category');

        return Telegram::sendMessage([
            'chat_id' => '407249739',
            'text' => 'Hello world!',
            'reply_markup' => $keyboard_markup
        ]);
    }

    public function getKeyboard($model='', $length = 1)
    {
        $names = [];

        if ($model != ""){
            $names = $model::pluck("title_". App::currentLocale())->all();
        }

        if ($model !== 'App\Menu'){
            $names[] = 'Go back';
            $names[] = 'Menu';
        }

        $keyboard = array_chunk($names, $length);

        return Keyboard::make([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);
    }
}
