<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Telegram\Bot\Laravel\Facades\Telegram;

class WebhookController extends Controller
{
    public function set(){
        $response = Telegram::setWebhook(['url' => route('webhook')]);

        if ($response[0]==true)
            return $response;

        return "Something went wrong!";
    }

    public function remove(){
        $response = Telegram::removeWebhook(['url' => route('webhook')]);

        dd($response);
    }

    public function info(){
        $response = Http::get('https://api.telegram.org/bot' . Telegram::getAccessToken() . '/getWebhookInfo');

        return $response;
    }
}
