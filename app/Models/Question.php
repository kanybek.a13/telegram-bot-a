<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Question extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function getTitleAttribute()
    {
        $title = 'title_' . App::currentLocale();
        return $this->$title;
    }

    public function getDescriptionAttribute()
    {
        $desc = 'description_' . App::currentLocale();
        return $this->$desc;
    }
}
