<?php

use App\Http\Controllers\TelegramController;
use App\Http\Controllers\WebhookController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Telegram\Bot\Laravel\Facades\Telegram;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('bot/get-updates', [TelegramController::class, 'getUpdates']);
Route::post('bot/send-message', [TelegramController::class, 'sendMessage']);


Route::post('/bot' . Telegram::getAccessToken(), [TelegramController::class, 'webhook'])->name('webhook');

Route::post('/webhook/set', [WebhookController::class, 'set']);
Route::post('/webhook/remove', [WebhookController::class, 'remove']);
Route::get('/webhook/info', [WebhookController::class, 'info']);

