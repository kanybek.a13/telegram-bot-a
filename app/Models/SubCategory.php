<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class SubCategory extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function questions()
    {
        return $this->hasMany(Question::class, 'sub_category_id', 'id');
    }

    public function getTitleAttribute()
    {
        $title = 'title_' . App::currentLocale();
        return $this->$title;
    }
}
