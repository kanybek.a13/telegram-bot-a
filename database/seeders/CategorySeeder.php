<?php

namespace Database\Seeders;

use App\Models\Category;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    public $faker;

    public function __construct(Faker $faker)
    {
        $this->faker = $faker;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [];

        foreach (range(1, 5) as $i) {
            $categories [] = Category::create([
                'title_ru' => "Категория №" . $i,
                'title_kk' => "Категория №" . $i,
            ]);
        }

        $sub_categories = [];

        foreach ($categories as $i => $category) {
            foreach (range(1, 5) as $i) {
                $sub_categories [] = $category->subCategories()->create([
                    'title_ru' => "Под Категория №" . $i,
                    'title_kk' => "Под Категория №" . $i,
                ]);
            }
        }

        $questions = [];

        foreach ($sub_categories as $sub_category) {
            foreach (range(1, 5) as $i) {
                $questions [] = $sub_category->questions()->create([
                    'title_ru' => $this->faker->sentence,
                    'title_kk' => $this->faker->sentence,
                    'description_ru' => $this->faker->text,
                    'description_kk' => $this->faker->text,
                ]);
            }
        }
    }
}
